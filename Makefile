cracker: chan.c main.c reverse.c safe.c sha256.c vec.c
	gcc -Wall -W -Werror -Wextra -Wpedantic -pedantic-errors -std=gnu99 \
	-O3 chan.c main.c reverse.c safe.c sha256.c vec.c -pthread -o cracker

clean:
	rm cracker
