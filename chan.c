#include <assert.h>
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <string.h>

#include "chan.h"
#include "safe.h"

void chan_init(struct chan *ch, size_t buf_size, size_t sizeof_slot,
               size_t num_producers, size_t num_consumers) {
	ch->buf = safe_malloc(buf_size * sizeof (void *));
	for (size_t i = 0; i < buf_size; i++) {
		ch->buf[i] = safe_malloc(sizeof_slot);
	}

	ch->buf_size = buf_size;
	ch->r = 0;
	ch->w = 0;
	ch->num_producers = num_producers;
	ch->num_consumers = num_consumers;

	pthread_mutex_init(&ch->mu, NULL);
	sem_init(&ch->num_empty, false, buf_size);
	sem_init(&ch->num_filled, false, 0);
	pthread_barrier_init(&ch->barrier, NULL, num_producers);
}

void chan_destroy(struct chan *ch) {
	for (size_t i = 0; i < ch->buf_size; i++) {
		free(ch->buf[i]); // for good measure
	}
	free(ch->buf);

	pthread_mutex_destroy(&ch->mu);
	sem_destroy(&ch->num_empty);
	sem_destroy(&ch->num_filled);
	pthread_barrier_destroy(&ch->barrier);
}

void chan_send(struct chan *ch, void *data, size_t n) {
	assert(data != NULL);

	sem_wait(&ch->num_empty);
	pthread_mutex_lock(&ch->mu);

	memcpy(ch->buf[ch->w++], data, n);
	ch->w %= ch->buf_size;

	pthread_mutex_unlock(&ch->mu);
	sem_post(&ch->num_filled);
}

bool chan_recv(struct chan *ch, void *res, size_t n) {
	sem_wait(&ch->num_filled);
	pthread_mutex_lock(&ch->mu);

	void *data = ch->buf[ch->r++];
	/* Copy unless it's the closing signal (see chan_close) */
	if (data != NULL) {
		memcpy(res, data, n);
	}
	ch->r %= ch->buf_size;

	pthread_mutex_unlock(&ch->mu);
	sem_post(&ch->num_empty);

	return data != NULL;
}

void chan_close(struct chan *ch) {
	/* Wait for all producers.
	 * That is, wait for all chan_send calls to finish so that they don't
	 * send to a closed channel and cause a null pointer indirection. */
	pthread_barrier_wait(&ch->barrier);

	/* Send the closing signal to each consumer.
	 * (Sending the signal to a single consumer would cause the others
	 * to wait indefinitely.) */
	for (size_t i = 0; i < ch->num_consumers; i++) {
		sem_wait(&ch->num_empty);
		pthread_mutex_lock(&ch->mu);

		free(ch->buf[ch->w]);
		ch->buf[ch->w] = NULL; /* send the closing signal */
		ch->w++;
		ch->w %= ch->buf_size;

		pthread_mutex_unlock(&ch->mu);
		sem_post(&ch->num_filled);
	}
}
