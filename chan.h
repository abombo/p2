#ifndef _CHAN_H
#define _CHAN_H

#include "stdbool.h"

/* A channel is used to synchronize interthread communication. */
struct chan {
	void **buf;
	size_t buf_size;
	size_t r, w; /* position of the consumers, producers in the buffer */
	size_t num_producers, num_consumers;
	pthread_mutex_t mu;
	sem_t num_empty, num_filled;
	pthread_barrier_t barrier;
};

void chan_init(struct chan *ch, size_t buf_size, size_t sizeof_slot,
               size_t num_producers, size_t num_consumers);

void chan_destroy(struct chan *ch);

/* Copies n bytes from data and send them to ch.
 * Blocks until a buffer slot is available.
 * (A slot is made available after channel initialization and
 * after a call to chan_recv has returned.)
 * Sending to a closed channel leads to undefined behavior. */
void chan_send(struct chan *ch, void *data, size_t n);

/* Copies n bytes from ch into res.
 * Blocks until a buffer slot is filled.
 * If ch is open, the data is stored in res.
 * Returns false if the channel was closed.
 * Receiving from a previously closed channel leads to undefined behavior. */
bool chan_recv(struct chan *ch, void *res, size_t n);

/* Sends the closing signal to each consumer through ch.
 * Blocks until all producers are ready to close the channel and
 * waits for each buffer slot to be available. */
void chan_close(struct chan *ch);

#endif
