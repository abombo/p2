/*
 * P2 -- Password Cracker
 * @author Aurélien Bombo
 */

#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "chan.h"
#include "reverse.h"
#include "safe.h"
#include "vec.h"

#define HASH_LEN 32 /* size of a sha256 hash */
#define PLAIN_LEN 16 /* max size of the inverse */

/* Ignore GCC warning. */
#define UNUSED(x) (void) (x)

/* Default values */
size_t NUM_THREADS = 1;
char LETTER = 'a';

/* Input file names */
char **files;
size_t num_files;

struct vec candidates;
size_t max_occ; /* current maximum number of occurrences of LETTER */

struct chan ch_hashes; /* holds arrays of uint8_t hashes */
struct chan ch_candidates; /* holds plaintext strings */

/* Reads the input files and emits hashes on ch_hashes.
 * Assumes all file sizes are multiples of 32 bytes. */
void *hash_producer(void *param) {
	UNUSED(param);
	for (size_t i = 0; i < num_files; i++) {
		FILE *f = fopen(files[i], "rb");
		if (f == NULL) {
			fprintf(stderr, "fopen %s: %s\n", files[i], strerror(errno));
			exit(1);
		}
		uint8_t hash[HASH_LEN];
		while (fread(hash, sizeof (uint8_t), HASH_LEN, f) == HASH_LEN) {
			chan_send(&ch_hashes, hash, HASH_LEN);
		}
		if (ferror(f)) {
			fprintf(stderr, "fread %s: error\n", files[i]);
			exit(1);
		}
		fclose(f);
	}
	chan_close(&ch_hashes);
	return NULL;
}

/* Consumes hashes from ch_hashes, reverse them, and emit candidates on
 * ch_candidates. */
void *hash_consumer(void *param) {
	UNUSED(param);
	uint8_t hash[HASH_LEN];
	while (chan_recv(&ch_hashes, hash, HASH_LEN)) {
		char plain[PLAIN_LEN + 1];
		if (reversehash(hash, plain, PLAIN_LEN)) {
			chan_send(&ch_candidates, plain, PLAIN_LEN + 1);
		}
	}
	chan_close(&ch_candidates);
	return NULL;
}

/* Consumes candidates from ch_candidates and appends them to candidates. */
void *candidate_consumer(void *param) {
	UNUSED(param);
	char candidate[PLAIN_LEN + 1];
	while (chan_recv(&ch_candidates, candidate, PLAIN_LEN + 1)) {
		size_t num_occ = 0;
		for (size_t i = 0, len = strnlen(candidate, PLAIN_LEN); i < len; i++) {
			if (candidate[i] == LETTER) {
				num_occ++;
			}
		}
		if (num_occ > max_occ) { /* discard current candidates */
			vec_destroy(&candidates);
			vec_init(&candidates);
			vec_append(&candidates, safe_strndup(candidate, PLAIN_LEN));
			max_occ = num_occ;
		} else if (num_occ == max_occ) { /* add candidate */
			vec_append(&candidates, safe_strndup(candidate, PLAIN_LEN));
		}
	}
	return NULL;
}

void check_err(int errnum) {
	if (errnum != 0) {
		fprintf(stderr, "%s\n", strerror(errnum));
		exit(1);
	}
}

void exit_usage(char **argv) {
	fprintf(stderr, "usage: %s [-t NTHREADS] [-l LETTER] ", argv[0]);
	fprintf(stderr, "FILE1 [FILE2 ... FILEN]\n");
	exit(2);
}

void parse_args(int argc, char **argv) {
	int c;
	while ((c = getopt(argc, argv, "l:t:")) != -1) {
		switch (c) {
		case 'l':
			LETTER = *optarg;
			break;
		case 't':
			NUM_THREADS = atoi(optarg);
			break;
		case '?':
			exit_usage(argv);
		default:
			abort(); /* unreachable */
		}
	}

	if (optind == argc) {
		exit_usage(argv);
	}

	files = &argv[optind];
	num_files = argc - optind;
}

int main(int argc, char **argv) {
	parse_args(argc, argv);

	/* Initialize resources */
	chan_init(&ch_hashes, NUM_THREADS, HASH_LEN * sizeof (uint8_t), 1, NUM_THREADS);
	chan_init(&ch_candidates, NUM_THREADS, (PLAIN_LEN + 1) * sizeof (char), NUM_THREADS, 1);
	vec_init(&candidates);

	/* Start hash producer */
	pthread_t thr_hash_producer;
	check_err(pthread_create(&thr_hash_producer, NULL, hash_producer, NULL));

	/* Start hash consumers/candidate producers */
	pthread_t thr_hash_consumers[NUM_THREADS];
	for (size_t i = 0; i < NUM_THREADS; i++) {
		check_err(pthread_create(&thr_hash_consumers[i], NULL, hash_consumer, NULL));
	}

	/* Start candidate consumer */
	pthread_t thr_candidate_consumer;
	check_err(pthread_create(&thr_candidate_consumer, NULL, candidate_consumer, NULL));

	/* Join threads */
	check_err(pthread_join(thr_candidate_consumer, NULL));
	check_err(pthread_join(thr_hash_producer, NULL));
	for (size_t i = 0; i < NUM_THREADS; i++) {
		check_err(pthread_join(thr_hash_consumers[i], NULL));
	}

	/* Print results and free resources */
	for (size_t i = 0; i < vec_len(&candidates); i++) {
		printf("%.*s\n", PLAIN_LEN, vec_get(&candidates, i));
	}

	/* Free resources */
	vec_destroy(&candidates);
	chan_destroy(&ch_hashes);
	chan_destroy(&ch_candidates);

	return 0;
}
