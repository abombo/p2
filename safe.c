#include <stdio.h>
#include <string.h>

#include "safe.h"

/* Aborts on malloc fail. */
void *safe_malloc(size_t n) {
	void *p = malloc(n);
	if (p == NULL) {
		fprintf(stderr, "malloc failed\n");
		abort();
	}
	return p;
}

/* Aborts on malloc fail. */
void *safe_realloc(void *ptr, size_t n) {
	void *p = realloc(ptr, n);
	if (p == NULL && n != 0) {
		fprintf(stderr, "realloc failed\n");
		abort();
	}
	return p;
}

/* Aborts on malloc fail. */
void *safe_strndup(const char *s, size_t n) {
	void *p = strndup(s, n);
	if (p == NULL) {
		fprintf(stderr, "strndup failed\n");
		abort();
	}
	return p;
}
