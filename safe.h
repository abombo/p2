#ifndef _SAFE_H
#define _SAFE_H

#include <stdlib.h>

/* Aborts on malloc fail. */
void *safe_malloc(size_t n);

/* Aborts on malloc fail. */
void *safe_realloc(void *ptr, size_t n);

/* Aborts on strndup fail. */
void *safe_strndup(const char *s, size_t n);

#endif
