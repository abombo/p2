#include <stdlib.h>

#include "safe.h"
#include "vec.h"

void vec_init(struct vec *l) {
	l->arr = NULL;
	l->length = 0;
	l->capacity = 0;
}

void vec_destroy(struct vec *l) {
	for (size_t i = 0; i < l->length; i++) {
		free(l->arr[i]);
	}
	free(l->arr);
}

void vec_append(struct vec *l, char *s) {
	/* Grow the array if needed (by a factor 1.5) */
	if (l->length == l->capacity) {
		l->capacity = (l->capacity != 0) ? (3*l->capacity)>>1 : 2;
		l->arr = safe_realloc(l->arr, l->capacity * sizeof (char *));
	}
	l->arr[l->length++] = s;
}

char *vec_get(struct vec *l, size_t i) {
	return l->arr[i];
}

size_t vec_len(struct vec *l) {
	return l->length;
}
