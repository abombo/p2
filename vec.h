#ifndef _VEC_H
#define _VEC_H

/* Dynamic array of strings.
 *
 * More space-efficient than a linked list:
 * for n strings, it requires at most 1.5*n pointers (it grows by a factor 1.5),
 * whereas a linked list needs 2*n pointers. */
struct vec {
	char **arr;
	size_t length, capacity;
};

void vec_init(struct vec *l);

void vec_destroy(struct vec *l);

/* Inserts s at the end of the array.
 * s must be freeable.
 * Amortized complexity is O(1). */
void vec_append(struct vec *l, char *s);

/* Returns the element at index i. */
char *vec_get(struct vec *l, size_t i);

/* Returns the length of the vector. */
size_t vec_len(struct vec *l);

#endif
